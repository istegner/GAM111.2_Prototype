using UnityEngine;

namespace AID
{
    /// <summary>
    /// This attribute can only be applied to fields because its
    /// associated PropertyDrawer only operates on fields (either
    /// public or tagged with the [SerializeField] attribute) in
    /// the target MonoBehaviour.
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Field)]
    public class InspectorButtonAttribute : PropertyAttribute
    {
        public readonly string MethodName, ButtonText;

        public InspectorButtonAttribute(string MethodName, string ButtonText)
        {
            this.MethodName = MethodName;
            this.ButtonText = ButtonText;
        }
    }
}