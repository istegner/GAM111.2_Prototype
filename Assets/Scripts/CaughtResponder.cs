﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CaughtResponder : MonoBehaviour
{
    public BattleConfig battleConfig;
    public TrainerConfig playerTrainer;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnCaught(GameObject enemy)
    {
        //battleConfig.enemy = enemy.GetComponent<EnemyStateController>().trainerConfig;
        battleConfig.enemy = enemy;
        battleConfig.EnemyPos = enemy.transform.position;
        battleConfig.playerPos = gameObject.transform.position;
        battleConfig.player = playerTrainer;
        SceneManager.LoadScene("Battle");
    }
}
