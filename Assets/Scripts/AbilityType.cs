﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AbilityType
{
    Physical,
    Special,
    Heal,
    DefUp,
    SpeedUP,
    DefDown,
    SpeedDown
}
