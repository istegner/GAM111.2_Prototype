﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Determines the strength and results and type of an ability of a monster.
/// Also contains its effect data
/// </summary>
[CreateAssetMenu(menuName = "BATTLE/AbilitySO")]
public class AbilitySO : ScriptableObject
{
    //public ElementalType elementalType = ElementalType.Normal;
    //public AbilityType abilityType = AbilityType.Physical;
    public int dmg;
    //public int manaCost;

    public AudioClip audioClip;
    public ParticleSystem particleSystem;
}
