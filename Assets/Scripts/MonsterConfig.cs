﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Determines the abilities and starting stats of a monster, should not be edited at runtime
/// </summary>
[CreateAssetMenu(menuName = "BATTLE/MonsterConfig")]
public class MonsterConfig : ScriptableObject
{
    public MonsterStats stats;

    public List<AbilitySO> abilities;
}
