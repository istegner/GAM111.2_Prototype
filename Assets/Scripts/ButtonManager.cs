﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    public Text gameName;
    public Text versionNumber;
    public Text loadingText;

    // Use this for initialization
    void Start()
    {
        gameName.text = Application.productName;
        versionNumber.text = "v" + Application.version;
    }

    public void StartButton()
    {
        loadingText.enabled = true;
        SceneManager.LoadScene(1);
    }

    public void QuitButton()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
