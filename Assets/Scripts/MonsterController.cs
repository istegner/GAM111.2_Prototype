﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents the actual monster in the world and its current stats
/// </summary>
public class MonsterController : MonoBehaviour
{
    public MonsterStats currentStats;
    public MonsterConfig monsterConfig;
    public int currentAbilityIndex;
    
    public AbilitySO SelectedAbility
    {
        get
        {
            return monsterConfig.abilities[currentAbilityIndex];
        }
    }

    public void Init()
    {
        currentStats = monsterConfig.stats;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void HandleAttack(AbilitySO att)
    {
        //this needs to be fancier, needs to be better math
        //needs to also use effects
        currentStats.HP -= att.dmg;
    }
}
