﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "BATTLE/BattleConfig")]
public class BattleConfig : ScriptableObject
{
    public Vector3 playerPos;
    public Vector3 EnemyPos;
    public TrainerConfig player;
    //public TrainerConfig enemy;
    public GameObject enemy;
}
