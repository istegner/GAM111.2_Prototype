﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// 
/// 
/// Messages;
///     - OnCaught - send to target when chase has completed and has reached the target
/// </summary>
public class EnemyStateController : MonoBehaviour
{
    public float maxVisionDist = 5;
    public float moveSpeed = 5;
    public float minDistToExclaim = 2;
    public float maxFollorDistance = 10;
    public Transform target;

    public TrainerConfig trainerConfig;
    public MonsterConfig monsterConfig;

    NavMeshAgent agent;

    public Transform[] waypoints;
    private Transform waypoint;

    public enum State
    {
        Idle,
        Chase,
        Exclaim,
        Defeated
    }

    public State state = State.Idle;

    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.Idle:
                Update_Idle();
                break;
            case State.Chase:
                Update_Chase();
                break;
            case State.Exclaim:
                break;
            case State.Defeated:
                //does nothing
                break;
            default:
                break;
        }
        //Debug.Log(agent.destination);
    }

    private void Update_Chase()
    {
        //transform.position = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
        //transform.LookAt(target.transform);

        agent.destination = target.position;

        if (Vector3.Distance(transform.position, target.position) < minDistToExclaim)
        {
            state = State.Exclaim;
            //notify gamemanager that player dun caught
            //tell player to stop moving
            target.SendMessage("OnCaught", this.gameObject, SendMessageOptions.DontRequireReceiver);
        }
        else if(Vector3.Distance(transform.position, target.position) > maxFollorDistance)
        {
            state = State.Idle;
        }
    }

    private void Update_Idle()
    {
        if (waypoint == null || Vector3.Distance(transform.position, waypoint.position) < 2)
        {
            waypoint = waypoints[Random.Range(0, waypoints.Length)];
        }

        agent.destination = waypoint.position;
        RaycastHit hit = new RaycastHit();
        if(Physics.Raycast(transform.position, (target.position - transform.position).normalized, out hit, maxVisionDist))
        {
            if(hit.transform.CompareTag("Player"))
            {
                state = State.Chase;
                target = hit.transform;
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, target.position);
    }
}
