﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    public AudioClip overworld, battle;
    static AudioSource overworldAS, battleAS;

    static MusicManager inst;

    void Awake()
    {
        if(inst != null)
        {
            //i am a mistake, sorry, leaving now
            Destroy(gameObject);
            return;
        }

        //haha, we are the singleton and we shall be FOREVA!!!
        inst = this;
    }

    // Use this for initialization
    void Start () {
        DontDestroyOnLoad(gameObject);

        overworldAS = gameObject.AddComponent<AudioSource>();
        overworldAS.clip = overworld;
        overworldAS.Play();
        overworldAS.loop = true;

        battleAS = gameObject.AddComponent<AudioSource>();
        battleAS.clip = battle;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void ResumeOverworld()
    {
        LeanTween.value(0, 1, 3).setEase(LeanTweenType.easeInCubic).setOnUpdate((float v) => overworldAS.volume = v);
        LeanTween.value(1, 0, 2).setEase(LeanTweenType.easeOutCubic).setOnUpdate((float v) => battleAS.volume = v).setOnComplete(()=>battleAS.Stop());
    }

    public static void TransitionToBattle()
    {
        battleAS.volume = 0;
        battleAS.Play();
        LeanTween.value(1, 0, 3).setEase(LeanTweenType.easeOutCubic).setOnUpdate((float v) => overworldAS.volume = v);
        LeanTween.value(0, 1, 1).setEase(LeanTweenType.easeInCubic).setOnUpdate((float v) => battleAS.volume = v);
    }
}
