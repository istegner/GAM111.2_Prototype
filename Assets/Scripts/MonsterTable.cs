﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// Simple example of pushing data from MonsterConfig collection to a flat csv and back again.
/// 
/// A similar approach using json or xml or yaml would be easy to do to.
/// </summary>
[CreateAssetMenu(menuName = "BATTLE/MonsterTable")]
public class MonsterTable : ScriptableObject
{
    [AID.InspectorButton("ReloadFile", "LoadFromFile")]
    public bool ReloadFromFile;

    [AID.InspectorButton("SaveFile", "SaveCurrentToFile")]
    public bool save;


    public TextAsset csvFile;

    public List<MonsterConfig> monsters;

    [System.Serializable]
    public class FlatMonsterData
    {
        public string name;
        public int hp;
        public int def;
    }


    public void ReloadFile()
    {
        var flat = AID.DeadSimpleCSV.ConvertStringCSVToObjects<FlatMonsterData>(csvFile.text, true);

        //go through
        foreach (var item in flat)
        {
            var mon = monsters.Find(x => x.name == item.name);
            if(mon != null)
            {
                mon.stats.HP = item.hp;
                mon.stats.Defense = item.def;
#if UNITY_EDITOR
                EditorUtility.SetDirty(mon);
#endif
            }
        }
    }

    public void SaveFile()
    {
        var flat = new List<FlatMonsterData>();

        foreach (var item in monsters)
        {
            flat.Add(new FlatMonsterData() {
                name = item.name,
                hp = item.stats.HP,
                def = item.stats.Defense
            });
        }

        System.IO.File.WriteAllText(Application.dataPath + "monstertable.csv",
            AID.DeadSimpleCSV.CreateFromList<FlatMonsterData>(flat).GetAsCSVString(true));
    }
}
