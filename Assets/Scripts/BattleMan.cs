﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Reponsible for orchestrating the turns, battle state, UI and connecting other Turn based battle
/// elements to each other.
/// </summary>
[RequireComponent(typeof(BattleManUIController))]
public class BattleMan : MonoBehaviour
{
    //Discrete states the battle can be in
    public enum BattleState
    {
        Initialise,     //one time init of battle when first entered
        PreTurn,        //prep and notify that a round is about to start (lingering effects notified)
        PlayerTurn,     //player gets their turn
        AITurn,         //ai gets their turn
        PreResolve,     //prep and notify that resolve is about to happen
        ResolveRound,   //use abilities deal dmg, heal etc.
        PostRound,      //check for winners, prep and notify that a round completed
        ExitBattle      //one time clean up for returning to overworld
    }

    [SerializeField]
    private BattleManUIController battleManUIController;

    //so we can see it in the inspector but we use the property in code so we can trigger other method calls if needed
    [SerializeField]
    private BattleState _state = BattleState.Initialise;
    public BattleState State
    {
        get
        {
            return _state;
        }
        //eventually this will probably have safety and consequences.
        set
        {
            _state = value;
        }
    }

    public TrainerController trainerControllerA, trainerControllerB;

    public BattleConfig theBattle;


    void Start()
    {
        if (battleManUIController == null)
            battleManUIController = GetComponent<BattleManUIController>();
        if (battleManUIController == null)
            Debug.LogError("BattleMan has no BattleManUIController, cannot continue");
    }

    void Update()
    {
        switch (State)
        {
            case BattleState.Initialise:
                trainerControllerA.Init();
                trainerControllerB.Init();
                State = BattleState.PreTurn;
                break;
            case BattleState.PreTurn:
                trainerControllerA.OnPreTurn();
                trainerControllerB.OnPreTurn();
                //fire a global event here

                State = BattleState.PlayerTurn;
                break;
            case BattleState.PlayerTurn:
                break;
            case BattleState.AITurn:
                break;
            case BattleState.PreResolve:
                break;
            case BattleState.ResolveRound:
                DoResolveRound();

                State = BattleState.PostRound;
                break;
            case BattleState.PostRound:
                break;
            case BattleState.ExitBattle:
                break;
            default:
                break;
        }
    }

    void DoResolveRound()
    {
        //this needs to be able to take time

        //this needs to handle being a self ability

        trainerControllerB.CurrentMonster.HandleAttack(trainerControllerA.CurrentMonster.SelectedAbility);
        //should confirm that it is still alive
        trainerControllerA.CurrentMonster.HandleAttack(trainerControllerB.CurrentMonster.SelectedAbility);
    }
}

/*
 * BattleManager
	controls start and end of battle phase
	Fighters[] (Trainers)
	
	DealDamage()
		Effective table
		Ability Used
		Monster/Fighter 
			Slot
		Calc dmg, calc miss chance, etc.
		
	States
		PreFight
		ActionSelection
		Resolve
		PostFight
	
	OnBegin
		track current state
		Roll for Init
		Refresh UI
		Transition of Music
	
	OnStartRound
		Fighters flag no selected action unless blocked
	
	OnEndRound
		...
	
	OnEnd
		Get points
		Get dead
		Get back to menu
		Transition of Music

Effective Table

Ability

Monster/Fighter
	Ability List
	Current Select Ability
	hp
	ap
	etc.
	
UIManager
	panels
	buttons
	End Turn
		Routes to battleman
	UpdateUI
		this is called when required by events/battleman
 */
