﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicTester : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [ContextMenu("Overworld")]
    public void Overworld()
    {
        MusicManager.ResumeOverworld();
    }

    [ContextMenu("BattleStart")]
    public void BattleStart()
    {
        MusicManager.TransitionToBattle();
    }


}
