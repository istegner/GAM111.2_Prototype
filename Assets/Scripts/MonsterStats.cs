﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Value type object for the statistics of a monster.
/// 
/// Expect to have base stats in a MonsterConfig and the current stats in a MonsterController
/// </summary>
[System.Serializable]
public struct MonsterStats
{
    public string Name;
    public int HP;
    public int Defense;
}
