﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents one side of a turn based battle, hold monsters handles choices
/// & AI.
/// </summary>
public class TrainerController : MonoBehaviour
{
    //my line up
    public List<MonsterController> monsterPrefabs;
    //my actual monsters
    public List<MonsterController> myActualMonsters;
    public int currentMonsterIndex;

    public Transform spawnLoc;

    public MonsterController CurrentMonster
    {
        get
        {
            return myActualMonsters[currentMonsterIndex];
        }
    }

    public void Init()
    {
        //make all monsters
        //put in actualmonsters list
        //call Init on the Monster you just made
        for (int i = 0; i < monsterPrefabs.Count; i++)
        {
            var newMonster = Instantiate(monsterPrefabs[i]);
            newMonster.Init();
            if (i != 0)
                newMonster.gameObject.SetActive(false);
            myActualMonsters.Add(newMonster);

            newMonster.transform.SetParent(spawnLoc, false);
        }

        //deactivate all but 0th
    }

    public void OnPreTurn()
    {
        //reset has taken turn, throw new monster if current dead
        Debug.Log("Totally preped");
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
