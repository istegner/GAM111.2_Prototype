﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Reponsible for updating UI layouts for battleman and routing information back to battle man
/// </summary>
[RequireComponent(typeof(BattleMan))]
public class BattleManUIController : MonoBehaviour
{
    [SerializeField]
    private BattleMan battleMan;

    [Header("Layout Elements")]
    //we have this so we can set it to inactive if we don't want the user to think they can click it
    [SerializeField]
    private Button endTurnButton;

    public BattleConfig battleConfig;

    public Text enemyName;
    public Text enemyHealth;


    private void Start()
    {
        if (battleMan == null)
            battleMan = GetComponent<BattleMan>();
        if (battleMan == null)
            Debug.LogError("BattleManUIController has no BattleMan, cannot continue");

        //enemyName.text = "Enemy: " + battleConfig.enemy.GetComponent<MonsterConfig>().stats.Name;
        enemyName.text = "Enemy: " + battleConfig.enemy.GetComponent<EnemyStateController>().monsterConfig.stats.Name;
    }

    public void OnEndTurnClicked()
    {
        //End Turn
    }

    void Update()
    {
        //enemyHealth.text = "Health: " + battleConfig.enemy.GetComponent<MonsterConfig>().stats.HP;
        enemyHealth.text = "Health: " + battleConfig.enemy.GetComponent<EnemyStateController>().monsterConfig.stats.HP;
    }
}
