﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ElementalType
{
    Normal,
    Ground,
    Electric,
    Fire,
    Water,
    Grass,
    Bug,
    Dragon,
    Ghost,
    Fairy,
    Dark,
    Light,
    Fighting,
    Cool,
    Funky,
    Classic,
    Dork,

}
