BattleManager
	controls start and end of battle phase
	Fighters[] (Trainers)
	
	DealDamage()
		Effective Table
		Ability used
		Monster/Fighter
			slot
		Calc dmg, calc miss chance, etc
	
	States
		PreFight
		ActionSelection
		Resolve
		PostFight
	
	CheckWinner
		
	OnBegin
		Track current state
		Roll for Init
		Refresh UI
		Transition of Music
	
	OnStartRound
		Fighters flag o selected action unless blocked
		
	OnEndRound
		...
	
	OnEnd
		Get points
		Get ded
		Get back to menu
		
Effective Table

Ability

Monster/Fighter
	Ability List
	Current selected ability
	hp
	ap
	etc
	
UIManager
	panels
	buttons
		End Turn
			Routes to battleman
	UpdateUI
		//this is called when required by event/battleman